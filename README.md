## About The Project
The application is a command line driven text search 
engine based on Aho-Corasick algorithm.
The advantages of the algorithm is that it matches all strings
simultaneously. What is more its complexity is linear
in the lenght of the strings plus the lenght of the searched 
text plus the number of output matches.
 
## Getting Started
Using sbt you can simply run the application with the 
argument which is the directory with the text files, that
you want to read. Please have a look on the following example:
```JS
run ".\\textFiles\\"
```

## The output 
The output consist of the information how many percent of 
given words were found in each text file of the directory. 
