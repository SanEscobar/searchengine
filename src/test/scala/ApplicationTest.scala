import Program.{FileContent, ResultOfSearch}
import org.scalatest.{FlatSpec, Matchers}

class ApplicationTest extends FlatSpec with Matchers {

  it should "find 3 .txt files in given dir" in new Context {
    Program.readFile(dir)
      .length shouldBe numberOfFilesInDir
  }

  it should "find 'Ala' in file containing 'Ala ma kota.'" in new Context {
    Program.searchWords(input, userInput, ac) shouldBe outputFromApp
  }

  trait Context {
    val dir = ".\\src\\test\\scala\\textFiles"
    val numberOfFilesInDir = 3

    val input = Array(FileContent("Name.txt", "Ala ma kota."))
    val userInput = "Ala"
    val ac = new AhoCorasick

    val outputFromApp = List(ResultOfSearch("Name.txt",100))
  }

}
