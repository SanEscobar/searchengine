import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable


class AhoCorasickTest extends FlatSpec with Matchers {

  it should "find a word Ala in given words" in new Context {
    ac.addWords(wordsToSearch)
    ac.setFail()
    ac.search(placeToSearch) shouldBe output

  }

  it should "find any word in given words" in new Context {
    ac.addWords(anyWordsToSearch)
    ac.setFail()
    ac.search(placeToSearch) shouldBe emptyOutput

  }

  trait Context{
    val ac = new AhoCorasick

    val wordsToSearch = Array("Ala", "Kasia", "Asia")
    val anyWordsToSearch = Array("Wiesia", "Kasia", "Asia")

    val placeToSearch = "Ala ma kota."

    val output = mutable.HashSet("Ala")
    val emptyOutput = mutable.HashSet()
  }
}
