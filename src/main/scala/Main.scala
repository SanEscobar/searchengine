import java.io.File
import scala.io.Source

object Main extends App {

  val ac = new AhoCorasick

  Program
    .searchWords(Program.readFile(args.mkString(" ")), Program.readFromUserInput(), ac)
    .map(x => println(x.fileName + ": " + x.percentOfSearch + "% "))
}

object Program {
  case class FileContent(fileName: String, fileText: String)
  case class ResultOfSearch(fileName: String, percentOfSearch: Int)

  def readFile(file: String): Array[FileContent] = {
    val d = new File(file)
    if (d.exists && d.isDirectory) {
      val fileList = d.listFiles
        .filter(_.isFile)
        .filter(name => name.getName.endsWith("txt"))
        .map(x => FileContent(x.getName, Source.fromFile(x).getLines().mkString))
      System.out.printf("%d files read in directory %s \n", fileList.length, file)
      fileList
    } else Array[FileContent]()
  }

  def readFromUserInput(): String ={
    System.out.println(s"search > ")
    val searchString = scala.io.StdIn.readLine()
    System.out.println(searchString)
    searchString
  }

  def searchWords(file: Array[FileContent], searchString: String, ac: AhoCorasick): List[ResultOfSearch] = {
    val numberOfWords = searchString.split(" ").length
    ac.addWords(searchString.split(" "))
    ac.setFail()

    file.map((x) => ResultOfSearch(x.fileName, 100 * ac.search(x.fileText).size / numberOfWords))
      .sortBy(_.fileName)(Ordering[String].reverse)
      .filter(_.percentOfSearch > 0)
      .take(10)
      .toList
  }
}