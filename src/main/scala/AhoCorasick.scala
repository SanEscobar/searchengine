import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

case class Node(state: Int,
                value: Byte,
                goto: mutable.Map[Byte, Int],
                var failure: Int,
                output: mutable.Set[String]) {

  override def toString: String = {
    state.toString + "," + value + "," + goto + "," + failure + "," + output
  }
}


class AhoCorasick {

  val trie: ArrayBuffer[Node] = ArrayBuffer(
    Node(state = 0,
      value = 0.asInstanceOf[Byte],
      goto = mutable.Map[Byte, Int](),
      failure = 0,
      output = mutable.Set[String]())
  )

  private def gotoState(node: Node, byte: Byte): Int = {
    node.goto.getOrElse(byte, -1)
  }

  def addWord(word: String): Unit = {
    var state = 0
    word.getBytes().foreach { t =>
      gotoState(trie(state), t) match {
        case -1 => val next = trie.size
          trie(state).goto += (t -> next)
          trie += Node(next, t, mutable.Map[Byte, Int](), 0, mutable.Set[String]())
          state = next
        case _ => state = gotoState(trie(state), t)
      }
    }
    trie(state).output += word
  }

  def addWords(words: Array[String]): Unit = {
    words.foreach {
      word => addWord(word)
    }
  }

  def setFail(): Unit = {
    val queue = mutable.Queue[Int]()
    trie(0).goto.foreach { case (a, s) =>
      queue += s
      trie(s).failure = 0
    }
    while (queue.nonEmpty) {
      val r = queue.dequeue()
      trie(r).goto.foreach { case (a, s) =>
        queue += s
        var state = trie(r).failure
        while (gotoState(trie(state), a) == -1 && state != 0)
          state = trie(state).failure
        val goto_a: Int = if (state == 0 && gotoState(trie(state), a) == -1) {
          0
        } else {
          trie(state).goto.getOrElse(a, 0)
        }
        trie(s).failure = goto_a
        trie(s).output ++= trie(trie(s).failure).output
      }
    }
  }

  def search(str: String): mutable.Set[String] = {
    val resultSet = mutable.Set[String]()
    var node = trie(0)
    str.getBytes.foreach { a =>
      while (gotoState(node, a) == -1 && node.state != 0)
        node = trie(node.failure)
      node = node.state == 0 && gotoState(node, a) == -1 match {
        case true => trie(0)
        case _ => trie(gotoState(node, a))
      }
      if (node.output.nonEmpty)
        resultSet ++= node.output
    }
    resultSet
  }
}
